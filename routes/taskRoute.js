const express = require("express")

const router = express.Router();

const taskController = require("../controllers/taskControllers")



router.get("/",(req, res) =>{
	taskController.getAllTasks().then(resultFromController => res.send(resultFromController))
})


router.post("/",(req,res)=>{
	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController))
})

//notice the colon in the calling. this will make a dynamic variable 
router.delete('/:task',(req,res) => {
	console.log(req.params.task);
	let taskId = req.params.task;
	//res.send('Deleted');
	taskController.deleteTask(taskId).then(deleteResults => res.send(deleteResults));
})

router.put('/:task',(req,res) => {

	let taskID = req.params.task;
	console.log(taskID);
	taskController.taskCompleted(taskID).then(results => res.send(results));

});

router.put('/:task/pending', (req, res) => {
		let tID = req.params.task;
		//console.log(tId);
		taskController.taskPending(tID).then(outcome => {
			res.send(outcome);
		})
});


module.exports = router;

