const Task = require("../models/task");





//retrieve
module.exports.getAllTasks = () => {

	return Task.find({}).then(result =>{
		return result
	})
} 

//create
module.exports.createTask = (requestBody) => {

	let newTask = new Task({
		name: requestBody.name
	})

	return newTask.save().then((task,error) => {
		if(error){
			return false
		}
		else {
			return task
		}
	})
}

//destroy
module.exports.deleteTask = (taskId) => {
	return Task.findByIdAndRemove(taskId).then((fullfilled, rejected) => {

		if (fullfilled) {
			return 'The Task has been successfully removed';
		} 
		else {
			return 'Failed to remove Task';
		};

	});
};

//updatetoComplete
module.exports.taskCompleted = (taskID) => {
	return Task.findById(taskID).then((found, err) => { 
		if (found) {
			console.log(found);
			found.status = 'Completed';
			return found.save().then((updatedTask,saveErr) => {
				if (updatedTask) {
					return 'task has been updated';
				} 
				else {
					return 'Error in modification';
				};

			});
		} 
		else {
			return 'No found Match';
		};

	});
};

//updatetoPending
module.exports.taskPending = (tID) => {
	return Task.findById(tID).then((change, err) => {
		if (change) {
			change.status = 'Pending'
			return change.save().then((taskUpdated, err) =>
			{
				if (taskUpdated) {
					return `task ${taskUpdated.name} was updated to Pending`
				} 
				else {
					return `Error when Updateing task`
				}
			})
		} 
		else {
			return 'Something Went Wrong';
		};
	});
};